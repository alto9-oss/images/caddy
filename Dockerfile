FROM caddy:2.4.5-builder AS builder
RUN xcaddy build --output /tmp/caddy --with github.com/ss098/certmagic-s3 --with github.com/caddy-dns/route53

FROM caddy:2.4.5-alpine
COPY --from=builder /tmp/caddy /usr/bin/caddy
.PHONY: auth build

repo = "registry.gitlab.com/alto9-oss/images/caddy"
platform = "linux/amd64"
tag = "latest"

auth:
	@docker login registry.gitlab.com
	
test:
	@docker buildx build --platform $(platform) -t $(repo):$(tag) .

build:
	@docker buildx build --platform $(platform) --push -t $(repo):$(tag) .
